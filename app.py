from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os  # provides ways to access the Operating System and allows us to read the environment variables

load_dotenv()

app = Flask(__name__)

uri = os.getenv('URI')
user = os.getenv("USERU")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=(user, password), database="neo4j")


def get_employees(tx, filter_params):
    query = "MATCH (m:Employee)"
    if filter_params:
        query += " WHERE "
        conditions = []
        for key, value in filter_params.items():
            conditions.append(f"m.{key} = '{value}'")
            query += " AND ".join(conditions)
    query += " RETURN m "
    results = tx.run(query).data()
    employees = [{'result': result['m']} for result in results]
    return employees


@app.route('/employees', methods=['GET'])
def get_employees_route():
    try:
        filter_params = request.args.to_dict()
        with driver.session() as session:
            employees = session.read_transaction(get_employees, filter_params)
        response = {'employees': employees}
        return jsonify(response)
    except Exception as e:
        return jsonify({'error': str(e)}), 500


def add_employee(tx, firstname, lastname, position, department):
    query = ("MATCH (dep:Department {name: $department})"
             "CREATE (e:Employee {firstname: $firstname, lastname: $lastname, position: $position}), "
             "(e)- [:WORKS_IN] -> (dep)")

    tx.run(query, firstname=firstname, lastname=lastname, position=position, department=department)


def is_unique(tx, firstname, lastname):
    query = "MATCH (n:Employee {firstname: $firstname, lastname: $lastname}) RETURN COUNT(n) AS count"
    result = tx.run(query, firstname=firstname, lastname=lastname).single()
    return result['count'] == 0


@app.route('/employees', methods=['POST'])
def post_employee_route():

    firstname = request.args.get('firstname')
    lastname = request.args.get('lastname')
    position = request.args.get('position')
    department = request.args.get('department')

    with driver.session() as session:
        if not (firstname is None or lastname is None or position is None or department is None):
            if session.read_transaction(is_unique, firstname, lastname):
                session.write_transaction(add_employee, firstname, lastname, position, department)
                response = {'status': 'success'}
            else:
                response = {'status': 'That person already exists'}

        else:
            response = {'status': 'Missing parameters'}
        return jsonify(response)


def update_employee(tx, employee_id, firstname, lastname, position, department):
    query_match = "MATCH (n:Employee) WHERE ID(n) = $employee_id RETURN n"
    result = tx.run(query_match, employee_id=employee_id).data()

    if not result:
        return None
    else:
        query_update = "MATCH (n:Employee) WHERE ID(n)=$employee_id"
        set_clauses = []

        if firstname is not None:
            set_clauses.append("n.firstname = $firstname")
        if lastname is not None:
            set_clauses.append("n.lastname = $lastname")
        if position is not None:
            set_clauses.append("n.position = $position")
        if len(set_clauses) > 0:
            query_update += " SET " + ", ".join(set_clauses)

        tx.run(query_update, firstname=firstname, lastname=lastname, position=position)

        if department is not None:
            query_delete = "MATCH (e:Employee)-[r]-() WHERE ID(n)=$employee_id DELETE r"
            tx.run(query_delete, employee_id=employee_id)

            query_create = (
                "MATCH (dep:Department{name:$department}),"
                "(e:Employee) WHERE ID(n) = $employee_id CREATE (e)-[:WORKS_IN]->(d)")

            tx.run(query_create, employee_id=employee_id, department=department)

        return {'Update Complete'}


@app.route('/employees/<string:employee_id>', methods=['PUT'])
def update_employee_route(employee_id):

    firstname = request.args.get('firstname')
    lastname = request.args.get('lastname')
    position = request.args.get('position')
    department = request.args.get('department')

    with driver.session() as session:
        employee = session.write_transaction(update_employee, int(employee_id), firstname,
                                             lastname, position, department)

    if not employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def delete_employee(tx, employee_id):
    query = "MATCH (n:Employee) WHERE ID(n) = $employee_id RETURN n"
    result = tx.run(query, employee_id=employee_id).data()

    if not result:
        return None
    else:
        is_manager = "MATCH(n:Employee)-[:MANAGES]->(dep:Department) WHERE ID(n) = $employee_id DETACH DELETE dep"
        tx.run(is_manager, employee_id=employee_id)
        query = "MATCH (n:Employee) WHERE ID(n) = $employee_id DETACH DELETE n"
        tx.run(query, employee_id=employee_id)
        return {'id': employee_id}


@app.route('/employees/<string:employee_id>', methods=['DELETE'])
def delete_employee_route(employee_id):
    with driver.session() as session:
        employee = session.write_transaction(delete_employee, int(employee_id))

    if not employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def get_subordinates(tx, employee_id):
    query = "MATCH (n:Employee) WHERE ID(n) = $employee_id RETURN n"
    result = tx.run(query, employee_id=employee_id).data()

    if not result:
        return None
    else:
        query_subordinates = ("MATCH (n:Employee) - [:MANAGES] -> (dep) <- [:WORKS_IN] - (e) "
                              "WHERE ID(n) = $employee_id AND NOT ID(e) = $employee_id RETURN e")
        results = tx.run(query_subordinates, employee_id=employee_id).data()
        subordinates = [{'firstname': result['e']['firstname'], 'lastname': result['e']['lastname'],
                         'position': result['e']['position']} for result in results]
    return subordinates


@app.route('/employees/<string:employee_id>/subordinates', methods=['GET'])
def get_subordinates_route(employee_id):
    with driver.session() as session:
        subordinates = session.read_transaction(get_subordinates, int(employee_id))

    if not subordinates:
        response = {'message': 'Employee not found or is not a manager'}
        return jsonify(response), 404
    else:
        response = {'subordinates': subordinates}
        return jsonify(response)


def get_department_employees(tx, department_id):
    query = "MATCH (dep:Department) WHERE ID(dep) = $department_is RETURN dep"
    result = tx.run(query, department_id=department_id).data()

    if not result:
        return None
    else:
        query = "MATCH (dep:Department)<-[:WORKS_IN]-(e:Employee) WHERE ID(dep) = $department_id RETURN e"
        results = tx.run(query, department_id=department_id)
        employees = [{'result': dict(result['e'])} for result in results]
        return employees


@app.route('/employees/<int:departmentID>/employees', methods=['GET'])
def get_department_employees_route(department_id):
    with driver.session() as session:
        employees = session.write_transaction(get_department_employees, department_id)

    if not employees:
        response = {'message': 'Department not found'}
        return jsonify(response), 404
    else:
        response = {'employees': employees}
        return jsonify(response)


def get_department_stats(tx, employee_id):
    query = """
    MATCH (n:Employee)-[:WORKS_IN]->(dep:Department)
    OPTIONAL MATCH (dep)<-[:WORKS_IN]-(e:Employee)
    OPTIONAL MATCH (dep)<-[:MANAGES]-(m:Employee)
    WHERE ID(n) = $employee_id
    RETURN
      dep.name AS departmentName,
      COUNT(e) AS numberOfEmployees,
      m.firstname AS managerFirstname
      m.lastname AS managerLastname
    """
    result = tx.run(query, employee_id=employee_id).data()
    return result[0] if result else None


@app.route('/employees/<int:employeeID>/departmentInfo', methods=['GET'])
def get_department_info_route(employee_id):
    with driver.session() as session:
        stats = session.read_transaction(get_department_stats, employee_id)

    if not stats:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {
            'departmentName': stats['departmentName'],
            'numberOfEmployees': stats['numberOfEmployees'],
            'departmentManager': stats['managerFirstname'] + ' ' + stats['managerLastname']
        }
        return jsonify(response)


def get_departments(tx, filter_by, filter_value, sort_by, sort_order):
    query = "MATCH (dep:Department) "

    if filter_by and filter_value:
        query += f" WHERE ANY(x IN [d.{filter_by}] WHERE x CONTAINS '{filter_value}') "

    query += " RETURN d "

    if sort_by:
        if sort_order is None:
            sort_order = "asc"
        query += f" ORDER BY d.{sort_by} {sort_order}"

    results = tx.run(query).data()

    departments = [{'name': result['dep']['name']} for result in results]
    return departments


@app.route('/departments', methods=['GET'])
def get_departments_route():
    try:

        filter_by = request.args.get('filter_by')
        filter_value = request.args.get('filter_value')
        sort_by = request.args.get('sort_by')
        sort_order = request.args.get('sort_order')

        with driver.session() as session:
            departments = session.read_transaction(get_departments, filter_by, filter_value, sort_by, sort_order)

        response = {'departments': departments}
        return jsonify(response)
    except Exception as e:
        return jsonify({'error': str(e)}), 500


if __name__ == '__main__':
    app.run()
